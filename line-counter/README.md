# Line counter

This script takes a path as an argument and returns the number of lines in that file/folder.

### Challenge Details

__**Line counter**__

**Difficulty:** Club Penguin

Write a program that takes a filename as a command-line argument and prints the number of lines in that file. If the provided argument does not refer to a file, an error should be printed.

**Bonus 1:** Improve your program by counting the lines in all the files within a folder if a folder is specified as a command-line argument instead of a file.

**Research topics:** `sys.argv`, `open()`, `os.path.exists()`, `os.path.isfile()`, `os.listdir()`