import sys
import os

path = sys.argv[1]

if len(sys.argv) < 2:
    print ("You must provide a path.")

else:
    if os.path.isfile(path):
        file = open (path, "r")
        lines = 0
        for line in file:
            lines += 1
        print (lines)
    elif os.path.isdir (path):
        print ("Path is a directory, getting lines from all files in the directory.")
        dirs = os.listdir (path)
        lines = 0
        for file in dirs:
            if os.path.isfile(path + file):
                opened = open(path + file)
                for line in opened:
                    lines += 1
        print (lines)
    else:
        print (path + " is not a valid file or folder.")